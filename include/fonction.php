<?php
/**
* Fonction qui génère un input type select avec toutes les villes
* @param $id id du select
* @param $ville code de la ville pour selected
* @param $civilite code de la civilite pour selected
* @return code HTML à afficher
*/
function selectVille($id, $ville) {
    global $bdd;
    $retour = "<select class=\"form-control\" id=\"$id\" name=\"$id\">\n";
    try {
        $requete = 'select code, nom from ville';
        foreach($bdd->query($requete) as $ligne) {
            if($ville == $ligne['code']) {
                $retour .= '<option value='.$ligne['code'].' selected="selected">'. $ligne['nom'].'</option>'."\n";
            } else {
                $retour .= '<option value='.$ligne['code'].'>'. $ligne['nom'].'</option>'."\n";
            }
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
    }
    $retour .= "</select>";
    return $retour;
}

function selectCivilite($id, $civilite) {
    global $bdd;
    $retour = "<select class=\"form-control\" id=\"$id\" name=\"$id\">\n";
    try {
        $requete = 'select code, libelle from civilite';
        foreach($bdd->query($requete) as $ligne) {
            if($civilite == $ligne['code']) {
                $retour .= '<option value='.$ligne['code'].' selected="selected">'. $ligne['libelle'].'</option>'."\n";
            } else {
                $retour .= '<option value='.$ligne['code'].'>'. $ligne['libelle'].'</option>'."\n";
            }
        }
    } catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
    }
    $retour .= "</select>";
    return $retour;
    }

function menuActif($menu) {
    $ecran = basename($_SERVER['SCRIPT_FILENAME'], ".php");
    if($ecran == $menu) {
        return "active";
    }
}

function afficheMessages() {
    $retour = '';
    if(!empty($_SESSION['MSG_OK'])) {
        $retour .= '<div class="alert alert-success">' . $_SESSION['MSG_OK'] .
        '</div>'."\n";
        unset($_SESSION['MSG_OK']);
    }
    if(!empty($_SESSION['MSG_KO'])) {
        $retour .= '<div class="alert alert-danger">' . $_SESSION['MSG_KO'] .
        '</div>'."\n";
        unset($_SESSION['MSG_KO']);
    }
    return $retour;
}
