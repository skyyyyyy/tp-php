<?php 
  require_once('../include/fonction.php');
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="../public/index.php">
    <img src="../include/logo.jpg" width="60" height="60" class="d-inline-block align-top" alt="">
    Mon tp php
  </a>
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link <?php echo menuActif('listeFournisseur'); ?>" href="../public/listeFournisseur.php">Liste des fournisseurs</a>
    </li>
    <li class="nav-item">
      <a class="nav-link <?php echo menuActif('listeVille'); ?>" href="../public/listeVille.php">Liste des villes</a>
    </li>
  </ul>
</nav>