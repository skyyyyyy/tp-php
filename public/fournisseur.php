<?php
session_start();
require_once('../include/connexion.php');
require_once('../include/fonction.php');

$id = (isset($_GET['id']))?$_GET['id']:0;

if($id == 0) {
    header("Location:/tp-php/public/listefournisseur.php");
    die();
}
// Si on à cliqué sur "Modifier"
if(isset($_POST['Modifier'])) {
    $_SESSION['MSG_KO']='';
    if(empty($_POST['adresse1'])) {
        $_SESSION['MSG_KO'] .= "L'adresse est obligatoire<br>" ;
        }
    if(strlen($_POST['nom'])<3) {
        $_SESSION['MSG_KO'] .= "Le nom doit contenir plus de 3 lettres<br>" ;
        }
        $requete = $bdd->prepare('SELECT count(*) as cpt FROM fournisseur where nom = :nom');
        $requete->execute(['nom'=>$_POST['nom']]);
        $compteur = $requete->fetch();
    if($compteur['cpt'] == 1) {
        $_SESSION['MSG_KO'] .= "le nom (".$_POST['nom'].") est déjà pris<br />";
        }
    else{
        try {
        $requete = $bdd->prepare('update fournisseur
        SET nom = :nom
        , adresse1 = :adresse1
        , adresse2 = :adresse2
        , ville    = :ville
        , contact  = :contact
        , civilite = :civilite
        where code = :code');
        $requete->execute(array('nom' => $_POST['nom']
        , 'adresse1' => $_POST['adresse1']
        , 'adresse2' => $_POST['adresse2']
        , 'ville'    => $_POST['ville']
        , 'contact'  => $_POST['contact']
        , 'civilite' => $_POST['civilite']
        , 'code' => $_POST['code']
        ));
        } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();
        }
        $_SESSION['MSG_OK'] = "Modification bien enregistrée";
}   }
if(isset($_POST['Annuler'])) {
    header("Location:/tp-php/public/listefournisseur.php");
    die();
}

try {
    $requete = $bdd->prepare('SELECT nom, code, adresse1, adresse2, ville, contact, civilite from fournisseur where code = ?');
    $requete->execute(array($id));
    $fournisseur = $requete->fetch();
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>fournisseur<?php echo $fournisseur['nom']; ?></title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/mon_style.css" rel="stylesheet">
</head>

<body>
<?php
include("../include/menu.php");
echo afficheMessages();
?>
<div class="container">
    <h1>fournisseur <?php echo $fournisseur['nom']; ?></h1>
</div>

<form method="post" class="form-inline">
    <div class="container">
        <div class="form-group">
            <label for="nom">Nom :</label>
                <input type="text" class="form-control" id="nom" name="nom" value = "<?php echo $fournisseur['nom']; ?>">
        </div>
        <div class="form-group">
            <label for="adresse1">Adresse1 :</label>
                <input type="text" class="form-control" id="adresse1" name="adresse1" value = "<?php echo $fournisseur['adresse1']; ?>">
            </div>
        <div class="form-group">
            <label for="adresse2">Adresse2 :</label>
                <input type="text" class="form-control" id="adresse2" name="adresse2" value = "<?php echo $fournisseur['adresse2']; ?>">
        </div>
        <div class="form-group raw">
            <label class="col-form-label col-sm-2"  for="ville">Ville :</label>
                <div class="col-sm-10">
                    <?php echo selectVille('ville', $fournisseur['ville']); ?>
                </div>
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Contact :</label>
                <input type="text" class="form-control" id="contact" name="contact" value = "<?php echo $fournisseur['contact']; ?>">
        </div>
        <div class="form-group raw">
            <label class="col-form-label col-sm-2"  for="civilite">Civilite :</label>
            <div class="col-sm-10">
                <?php echo selectCivilite('civilite', $fournisseur['civilite']); ?>
            </div>
        </div>
    </div>

    <div class="form-group row float-right">
        <input type="submit" class="btn btn-default" name="Annuler"value="Annuler">
        <input type="submit" class="btn btn-primary" name="Modifier"value="Modifier" id="Modifier">
        <input type="hidden" name="code" id="code" value="<?php echo $fournisseur['code']; ?>">
    </div>
</form>
</body>
</html>