<?php
require_once('../include/fonction.php');
include("../include/connexion.php");
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <?php
            include("../include/menu.php");
        ?>
        <meta charset="utf-8">
        <title>Liste des fournisseurs</title>
        <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="../css/mon_style.css" rel="stylesheet">
        <link href="../node_modules/datatables.net-bs5/css/dataTables.bootstrap5.css" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <h1>Les fournisseurs</h1>
<?php

/**
 * Page qui affiche la liste de toutes les villes
 */

$requete = 'SELECT fournisseur.code as id
            , fournisseur.nom
            , codepostal
            , civilite.libelle AS civilite
            , ville.nom AS ville
            , contact
            FROM civilite
            , fournisseur
            , ville 
            WHERE civilite.code = fournisseur.civilite
            AND fournisseur.ville = ville.code';
?>
    <table class="table table-hover table-striped" style="width:100%" id="fournisseur">
        <thead>
            <tr>
                <th>civilite</th>
                <th>Nom</th>
                <th>contact</th>
                <th>Code postal</th>
                <th>ville</th>
            </tr>
        </thead>
        <tbody>
<?php
try {
    foreach($bdd->query($requete) as $ligne) {
        $url = 'fournisseur.php?id=' . $ligne['id'];
        echo '<tr class="clickable-row" data-href="' . $url . '">';
        echo '<td>' . $ligne['civilite'] . '</td>';
        echo '<td>' . $ligne['nom'] . '</td>';
        echo '<td>' . $ligne['contact'] . '</td>';
        echo '<td>' . $ligne['codepostal'] . '</td>';
        echo '<td>' . $ligne['ville'] . '</td>';
        echo "</tr>\n";
    }
} catch (PDOException $e) {
    echo 'Erreur !: ' . $e->getMessage() . '<br>';
    die();
}

?>
        </tbody>
    </table>
    </div>
        <script src="../node_modules/jquery/dist/jquery.min.js"></script>
        <script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="../node_modules/dataTables.net-bs5/js/dataTables.bootstrap5.min.js"></script>
        <script>
            $(document).ready(function () {
                $('#fournisseur').DataTable({
                    language: {
                        url: '../include/fr_FR.json'
                    }
                });
            });
        </script>
        <script>
            $(document).ready(function($) {
                $(".clickable-row").click(function() {
                    window.location = $(this).data("href");
                });
            });
            $(".dataTables_paginate").bind( "click", '.paginate_button', function() {
            alert("."); $(".paginate_button").css("font-weight", "bold");
            });
        </script>
    </body>
</html>