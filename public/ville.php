<?php
require_once('../include/connexion.php');
require_once('../include/fonction.php');
include("../include/menu.php");

$id = (isset($_GET['id']))?$_GET['id']:0;
if($id == 0) {
    header("Location:/tp-php/public/listeville.php");
    die();
}
$pays = ['France', 'Andorre', 'Monaco'];

if(isset($_POST['Modifier'])) {
    $_SESSION['MSG_KO'] = '';
    
    if (empty($_POST['pays']) || !in_array($_POST['pays'], $pays)) {
        $_SESSION['MSG_KO'] .= "Le pays doit être la France, Andorre ou Monaco!<br>";
    }
    
    if (strlen($_POST['nom']) < 2) {
        $_SESSION['MSG_KO'] .= "Le nom doit contenir plus de 2 lettres<br>";
    }
    
    if (!is_numeric($_POST['codepostal']) || strlen($_POST['codepostal']) != 5) {
        $_SESSION['MSG_KO'] .= "Le code postal doit être égal à 5 chiffres<br>";
    }
    $requete = $bdd->prepare('SELECT COUNT(*) as cpt FROM ville WHERE nom = :nom');
    $requete->execute(['nom' => $_POST['nom']]);
    $compteur = $requete->fetch();
    
    if ($compteur['cpt'] == 1) {
        $_SESSION['MSG_KO'] .= "Le nom (" . htmlspecialchars($_POST['nom']) . ") est déjà pris<br>";
    } else {
        try {
            $requete = $bdd->prepare('UPDATE ville SET nom = :nom, codepostal = :codepostal, pays = :pays WHERE code = :code');
            $requete->execute([
                'nom' => $_POST['nom'],
                'codepostal' => $_POST['codepostal'],
                'pays' => $_POST['pays'],
                'code' => $_POST['code']
            ]);
            $_SESSION['MSG_OK'] = "Modification bien enregistrée";
        } catch (PDOException $e) {
            echo "Erreur !: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}
if(isset($_POST['Annuler'])) {
    header("Location:/tp-php/public/listeville.php");
    die();
}
try {
    $requete = $bdd->prepare('SELECT code, nom, codepostal, pays FROM ville where code = ?');
    $requete->execute(array($id));
    $ville = $requete->fetch();
} catch (PDOException $e) {
    print "Erreur !: " . $e->getMessage() . "<br/>";
    die();
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ville<?php echo $ville['nom']; ?></title>
    <link href="../node_modules/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/mon_style.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>ville <?php echo $ville['nom']; ?></h1>
</div>
<form class="form-inline">
    <div class="container">
        <div class="form-group">
            <label for="exampleFormControlInput1">Nom :</label>
                <input type="text" class="form-control" id="exampleFormControlInput1"  value = "<?php echo $ville['nom']; ?>">
        </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Code postal :</label>
                <input type="text" class="form-control" id="exampleFormControlInput1"  value = "<?php echo $ville['codepostal']; ?>">
            </div>
        <div class="form-group">
            <label for="exampleFormControlInput1">Pays :</label>
                <input type="text" class="form-control" id="exampleFormControlInput1"  value = "<?php echo $ville['pays']; ?>">
            </div>
    </div>

    <div class="form-group row float-right">
        <input type="submit" class="btn btn-default" name="Annuler"value="Annuler">
        <input type="submit" class="btn btn-primary" name="Modifier"value="Modifier" id="Modifier">
        <input type="hidden" name="code" id="code" value="<?php echo $ville['code']; ?>">
    </div>

</body>
</html>